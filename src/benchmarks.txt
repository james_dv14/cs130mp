The running times of the two inversion methods at three different matrix sizes are compared below.

---

Adjugate and Matrix of Cofactors

  8x8 =  0.180 seconds
  9x9 =  1.392 seconds
10x10 = 15.223 seconds

---

Gauss-Jordan Elimination

  400x400 =  0.777 seconds
  700x700 =  4.754 seconds
1000x1000 = 14.928 seconds

---

The Gauss-Jordan method is clearly faster. This is primarily because it doesn't involve getting the determinant, which is by itself a computationally expensive recursive process. 

However, the adjugate/cofactor method exacerbates this by having to compute the determinant once for every cell of the matrix. This effectively adds another dimension to the time complexity, going from an O(n^3) to an O(n^4) operation.

---

In practical terms, the program can be useful for students even with the adjugate/cofactor method, as it is still much faster than doing the computations by hand. However, it is very slow with any matrix larger than 10x10.

For heavy-duty applications, the Gauss-Jordan method is the clear winner. It can invert matrices with several hundred rows/columns in less than a second.

The limits of the program depend on what qualifies as "reasonable time". If it were being used for a video game, I would put the limit slightly below 400x400 as a delay of even one second would be undesirable. On the other hand, if it is alright to leave the program running while it handles a large dataset, matrices of size well over 1000x1000 are fine.