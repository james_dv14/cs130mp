package matrix;

import common.CopyHelper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Matrix {
  //********************************
  // Values
  //********************************
  public double[][] m;
  //********************************
  // Constructor
  //********************************
  public Matrix (double[][] matrix) {
    m = matrix;
  }
  //********************************
  // Accessors
  //********************************
  public double get(int r, int c) {
    return m[r][c];
  }
  
  public int rows() {
    return m.length;
  }
  
  public int cols() {
    return m[0].length;
  }
  //********************************
  // Static methods
  //********************************
  public static Matrix getIdentityMatrix(int size) {
    double[][] im = new double[size][size];
    for (int i = 0; i < size; i++)
      im[i][i] = 1;
    return new Matrix(im);
  }
  //********************************
  // Instance methods
  //********************************
  public Matrix getScalarMultiple(double scalar) {
    double[][] sm = new double[rows()][cols()];
    for (int r = 0; r < rows(); r++)
      for (int c = 0; c < cols(); c++)
        sm[r][c] = scalar * m[r][c];
    return new Matrix(sm);
  }
  
  public Matrix multiply(Matrix other) throws Exception {
    if (cols() != other.rows())
      throw new Exception("Incompatible matrix sizes.");
  
    other = other.getAdjugate();  
    double[][] prod = new double[rows()][other.rows()];
    for (int r = 0; r < rows(); r++)
      for (int c = 0; c < cols(); c++)
        for (int _r = 0; _r < other.rows(); _r++)
            prod[r][_r] += get(r, c) * other.get(_r, c);
    return new Matrix(prod);
  }
  
  public boolean isSquareMatrix() {
    return rows() == cols();
  }
  
  public Matrix getSubmatrix(int r_idx, int c_idx) {
    if (rows() > 2) {
      double[][] sub = new double[rows() - 1][cols() - 1];
      int sub_r = 0;
      for (int r = 0; r < rows(); r++) {
        if (r == r_idx) continue;
        int sub_c = 0;
        for (int c = 0; c < cols(); c++) {
          if (c == c_idx) continue;
          sub[sub_r][sub_c] = m[r][c];
          sub_c++;
        }
        sub_r++;
      }
      return new Matrix(sub);
    }
    else {
      return this;
    }
  }
  
  public double getDeterminant() throws Exception {
    if (!isSquareMatrix())
      throw new Exception("Not a square matrix.");
    
    if (rows() == 2) {
      return m[0][0] * m[1][1] - m[0][1] * m[1][0];
    }
    else {
      double det = 0.0;
      for (int c = 0; c < rows(); c++)
        det += Math.pow(-1.0, c) * m[0][c] * getSubmatrix(0, c).getDeterminant();
      return det;
    }
  }
  
  public Matrix getMatrixOfMinors() throws Exception {
    double[][] mom = new double[rows()][cols()];
    for (int r = 0; r < rows(); r++)
      for (int c = 0; c < cols(); c++)
        mom[r][c] = getSubmatrix(r, c).getDeterminant();
    return new Matrix(mom);
  }
  
  public Matrix getMatrixOfCofactors() throws Exception {
    double[][] _m = getMatrixOfMinors().m;
    double[][] moc = CopyHelper.deepCopy(_m);
    for (int r = 0; r < _m.length; r++)
      for (int c = 0; c < _m[r].length; c++)
        moc[r][c] *= Math.pow(-1.0, r) * Math.pow(-1.0, c);
    return new Matrix(moc);
  }
  
  public Matrix getAdjugate() {
    double[][] adj = new double[cols()][rows()];
    for (int r = 0; r < rows(); r++)
      for (int c = 0; c < cols(); c++)
        adj[c][r] = m[r][c];
    return new Matrix(adj);
  }
  
  public Matrix getInverse(InversionStrategy is) throws Exception {
    return is.getInverse(this);
  }
  //********************************
  // Row ops
  //********************************
  // Methods beginning with an underscore are mutator (in-place) versions. 
  // They are faster but will change the caller's values permanently.

  // Swap
  public Matrix swapRows(double[][] sr, int r1, int r2) {
    // Sophia
    for (int c = 0; c < cols(); c++) {
      sr[r1][c] += sr[r2][c];
      sr[r2][c] = sr[r1][c] - sr[r2][c];
      sr[r1][c] -= sr[r2][c];
    }
    return new Matrix(sr);
  }
  
  public Matrix swapRows(int r1, int r2) {
    return swapRows(CopyHelper.deepCopy(m), r1, r2);
  }
  
  public Matrix _swapRows(int r1, int r2) {
    return swapRows(m, r1, r2);
  }
  
  // Multiply
  public Matrix multiplyRow(double[][] mr, int r, double x) {
    // Mikaela
    for (int c = 0; c < cols(); c++) {
      mr[r][c] *= x;
    }
    return new Matrix(mr);
  }
  
  public Matrix multiplyRow(int r, double x) {
    return multiplyRow(CopyHelper.deepCopy(m), r, x);
  }
  
  public Matrix _multiplyRow(int r, double x) {
    return multiplyRow(m, r, x);
  }
  
  // Add
  public Matrix addRow(double[][] ar, int r1, int r2, double x) {
    // Alexandra
    for (int c = 0; c < cols(); c++)
      ar[r2][c] += x * m[r1][c];
    return new Matrix(ar);
  }
  
  public Matrix addRow(int r1, int r2, double x) {
    return addRow(CopyHelper.deepCopy(m), r1, r2, x); 
  }
  
  public Matrix _addRow(int r1, int r2, double x) {
    return addRow(m, r1, r2, x); 
  }
  //********************************
  // Representation
  //********************************
  @Override
  public String toString() {
    String str = "";
    for (int r = 0; r < rows(); r++) {
      String rowstr = "";
      for (int c = 0; c < cols(); c++)
        rowstr += String.format("%.2f", m[r][c]) + " ";
      str += rowstr.trim() + System.lineSeparator();
    }          
    return str.trim();
  }
}
