package matrix;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class CofactorAdjugateStrategy implements InversionStrategy {

  @Override
  public Matrix getInverse(Matrix m) throws Exception {
    double det = m.getDeterminant();
    if (det == 0)
      throw new Exception("That matrix has no inverse.");
    else
      return m.getMatrixOfCofactors().getAdjugate().getScalarMultiple(1.0 / det);
  }

}
