package matrix;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class GaussJordanStrategy implements InversionStrategy {

  /**
   * Adapted from:
   * http://people.ucsc.edu/~ptantalo/math21/Winter07/GaussJordan.java
   */
  @Override
  public Matrix getInverse(Matrix m) throws Exception {
    Matrix m2 = Matrix.getIdentityMatrix(m.rows());
    
    for (int c = 0; c < m.cols(); c++) {
      for (int r = c+1; r < m.rows(); r++) {
        if (m.get(r, c) != 0) {
          m._swapRows(c, r);
          m2._swapRows(c, r);
          // System.out.println("Swap r" + (c+1) + " and r" + (r+1));
          // System.out.println(m + System.lineSeparator());
          // System.out.println(m2 + System.lineSeparator());
        }
      }
      
      if (m.get(c, c) != 1.0) {
        double x = 1.0 / m.get(c, c);
        m._multiplyRow(c, x);
        m2._multiplyRow(c, x);
        // System.out.println("Multiply r" + (c+1) + " by " + x);
        // System.out.println(m + System.lineSeparator());
        // System.out.println(m2 + System.lineSeparator());
      }
      
      for (int r = c+1; r < m.rows(); r++) {
        if (m.get(r, c) != 0) {
          double x = -m.get(r, c) / m.get(c, c);
          m._addRow(c, r, x);
          m2._addRow(c, r, x);
          // System.out.println("Subtract " + x + "r" + (c+1) + " from r" + (r+1));
          // System.out.println(m + System.lineSeparator());
          // System.out.println(m2 + System.lineSeparator());
        }
      }
    }
    
    for (int c = m.cols() - 1; c >= 0; c--) {
      for (int r = c-1; r >=0; r--) {
        if (m.get(r, c) != 0) {
          double x = -m.get(r, c) / m.get(c, c);
          m._addRow(c, r, x);
          m2._addRow(c, r, x);
          // System.out.println("Subtract " + x + "r" + (c+1) + " from r" + (r+1));
          // System.out.println(m + System.lineSeparator());
          // System.out.println(m2 + System.lineSeparator());
        }
      }
    }
    
    return m2;
  }

}
