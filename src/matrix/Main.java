package matrix;

import common.InputHelper;
import common.OutputHelper;
import common.PromptHelper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Main {    
  static String in1;
  static String in2;
  static String out;
      
  public static void main(String[] args) throws Exception {
    boolean good = true;
    while (good) {
      System.out.println("Select an option:");
      System.out.println("1. Multiply 2 matrices");
      System.out.println("2. Invert a matrix (cofactor/adjugate)");
      System.out.println("3. Invert a matrix (Gauss-Jordan)");
      System.out.println("4. Quit");
      String choice = PromptHelper.promptMessage("");
      switch (choice) {
        case "1":
          multiply();
          break;
        case "2":
          invertCA();
          break;
        case "3":
          invertGJ();
          break;
        default:
          good = false;
      }
    }
  }
  
  public static void multiply() {
    in1 = PromptHelper.promptMessage("Enter the path of the first input (.csv): ");
    in2 = PromptHelper.promptMessage("Enter the path of the second input (.csv): ");
    out = PromptHelper.promptMessage("Enter the path of the output (.csv): ");
    try {
      Matrix m1 = InputHelper.getMatrixFromCSV(in1);
      Matrix m2 = InputHelper.getMatrixFromCSV(in2);
      Matrix result = m1.multiply(m2);
      OutputHelper.writeMatrixToFile(out, result);
      System.out.println("Success!" + System.lineSeparator());
    } 
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  
  public static void invertCA() {
    in1 = PromptHelper.promptMessage("Enter the path of the input (.csv): ");
    out = PromptHelper.promptMessage("Enter the path of the output (.csv): ");
    try {
      Matrix m = InputHelper.getMatrixFromCSV(in1);
      Matrix result = m.getInverse(new CofactorAdjugateStrategy());
      OutputHelper.writeMatrixToFile(out, result);
      System.out.println("Success!" + System.lineSeparator());
    } 
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  
  public static void invertGJ() {
    in1 = PromptHelper.promptMessage("Enter the path of the input (.csv): ");
    out = PromptHelper.promptMessage("Enter the path of the output (.csv): ");
    try {
      Matrix m = InputHelper.getMatrixFromCSV(in1);
      Matrix result = m.getInverse(new GaussJordanStrategy());
      OutputHelper.writeMatrixToFile(out, result);
      System.out.println("Success!" + System.lineSeparator());
    } 
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}