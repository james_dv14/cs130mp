package matrix;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public interface InversionStrategy {
  
  public Matrix getInverse(Matrix m) throws Exception;

}
