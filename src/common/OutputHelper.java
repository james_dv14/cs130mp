package common;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import matrix.Matrix;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class OutputHelper {
  static PrintWriter output;
  
  public static void writeMatrixToFile(String filepath, Matrix m) throws IOException {
    output = new PrintWriter(new FileWriter(new File(filepath)), false);
    
    for (int r = 0; r < m.rows(); r++) {
      for (int c = 0; c < m.cols(); c++) {
        output.print(String.format("%.2f", m.get(r, c)));
        if (c != m.cols() - 1) output.print(",");
      }
      if (r != m.rows() - 1) output.println();
    }
    
    output.flush();
    output.close();
  }

}
