package common;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import matrix.Matrix;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class InputHelper {
  static File file;
  static Scanner scanner;
  static ArrayList<String[]> rows;
  static int cols;
  
  public static Matrix getMatrixFromCSV(String filepath) throws FileNotFoundException, Exception {
    file = new File(filepath);
    scanner = new Scanner(file);
    
    rows = new ArrayList();
    cols = 0;
    
    while(scanner.hasNextLine()) {
      String line = scanner.nextLine();
      String[] row = line.split(",");
      if (cols < 2) cols = row.length;
      if (cols != row.length) throw new Exception("Ragged matrix.");
      rows.add(row);
    }
    scanner.close();
    
    double[][] m = new double[rows.size()][cols];
    for (int r = 0; r < rows.size(); r++)
      for (int c = 0; c < cols; c++)
        m[r][c] = Double.parseDouble(rows.get(r)[c]);
        
    return new Matrix(m);
  }
  
}
