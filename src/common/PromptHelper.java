package common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class PromptHelper { 
  /**
   * Returns console input the user provides in response to a given prompt.
   * @param prompt The prompt.
   * @return The console input as a string.
   */
  public static String promptMessage(String prompt) {
    String msg;
    
    if (prompt != null) 
      if (!prompt.isEmpty())
        System.out.print(prompt);
    
    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    try {
      msg = input.readLine();
    }
    catch (IOException e) {
      msg = "";
    }
    return msg;
  }

}
