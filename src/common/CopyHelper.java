package common;

import java.util.Arrays;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class CopyHelper {
  /**
   * http://stackoverflow.com/questions/1564832/how-do-i-do-a-deep-copy-of-a-2d-array-in-java
   */
  public static double[][] deepCopy(double[][] original) {
    if (original == null) return null;
    double[][] result = new double[original.length][];
    for (int i = 0; i < original.length; i++)
      result[i] = Arrays.copyOf(original[i], original[i].length);
    return result;
  }
}
