package common;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ThreadHelper {
  
  /**
   * Runs a Runnable in a thread.
   * @param r A Runnable.
   * @param name A name for the thread.
   */
  public static void start(Runnable r, String name) {
    Thread t = new Thread(r);
    t.setName(name);
    t.start();
  }

}
