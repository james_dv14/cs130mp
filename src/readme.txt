The MP's matrix module uses a simple interactive command-line interface. 

Upon running "java Main", the user is greeted with a list of options: multiply two matrices, invert a matrix using the adjugate and matrix of cofactors, or invert a matrix using Gauss-Jordan elimination. 

Upon choosing one, the user is asked to enter the file paths of the spreadsheets containing the input matrices, as well as the destination for the output. All files should be in CSV format.

The program will ask the user to input options for operations until they decide to quit.

The program will output logic-related errors to the command prompt. For example, it will recognize when two matrices are not the proper size for multiplication, or when the user attempts to get the determinant of a non-square matrix.