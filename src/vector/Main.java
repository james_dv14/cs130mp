package vector;

import common.PromptHelper;
import matrix.Matrix;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Main {
  
  public static void main(String[] args) throws Exception {
    double[][] bases = new double[3][];
    
    for (int i = 0; i < 3; i++)
      bases[i] = promptForR3Vector("Enter a vector in R3 (ex. 1, 1, 0): ");
    
    Matrix base_m = new Matrix(bases);
    
    if (base_m.getDeterminant() == 0)
      throw new Exception("Vectors must be linearly independent.");
    
    double[][] point = new double[1][];
    point[0] = promptForR3Vector("Enter a point in R3 (ex. 0, 0, 1): ");
    
    Matrix point_m = new Matrix(point);
    
    Matrix new_point_m = base_m.getAdjugate().multiply(point_m.getAdjugate());
    
    System.out.print("Result: ");
    System.out.println(matrixToPoint(new_point_m.getAdjugate()));
    
  }
  
  public static String matrixToPoint(Matrix matrix) throws Exception {
    if (matrix.m.length != 1)
      throw new Exception("Invalid matrix length.");
    
    String retstr = "(";
    for (double cell : matrix.m[0])
      retstr += cell + ", ";
    return retstr.substring(0, retstr.length() - 3) + ")";
  }
  
  public static double[] promptForR3Vector(String msg) throws Exception {
    String vector = PromptHelper.promptMessage(msg);
    String[] vector_arr = vector.split(",\\s*");
    
    if (vector_arr.length != 3)
      throw new Exception("Input must be in R3");
    
    double[] ret_arr = new double[3];
    try {
      for (int i = 0; i < 3; i++)
        ret_arr[i] = Double.parseDouble(vector_arr[i]);
    }
    catch(NumberFormatException e){
      e.printStackTrace();
      System.out.println("Please enter valid numbers.");
      System.exit(0);
    }
    
    return ret_arr;
  }

}
